# Atari 800 Power LED Cover

This is a 3D printable cover for the Atari 800 power indicator LED.

I had the fully functional guts of an Atari 800 on the shelf for a very long time.
It was a donation by a former collegue.
The case was missing though.

One day I managed to purchase a case by auction.
However, the cover for the power indicator LED was missing.

Therefore, I created this project to print my own.
Maybe you are missing a LED too.

Holger Zahnleiter, 2023-06-27

## The Files

- `atari_800_power_led_cover.f3d` - Original Fusion 360 CAD file.
- `export.step` - STEP export.
- `export.stl` - STL export.
- `cr10max_0.4mm.gcode` - Sliced for Creality CR-10 Max, using a 0.4mm nozzle.

## Assembly

This implements the most simple approach.
Just pop the print into place and glue with the least ammount of hot glue.

I have printed my part with natural PLA which is semi transparent.
This allows light to shine through.

![missing power indicator led cover](img/missing.png "Missing Power Indicator LED Cover")

![printed power indicator led cover](img/printed.png "Printed Power Indicator LED Cover")

![mounted power indicator led cover](img/mounted.png "Mounted Power Indicator LED Cover")

![power indicator led cover](img/done.png "Power Indicator LED Cover")

## Disclaimer

This project is non-profit, free and open source.
I am not liable for anything.
